import {Router } from 'express';
import RecipeController from "../controllers/RecipeController";
import UserController from "../controllers/UserController";
import IngredientController from "../controllers/IngredientController";

const router = Router();
router.get('/', function (req, res) {
    res.send("<h1>Hello</h1>")
});

router.post('/user', UserController.create);
router.get('/user/:id', UserController.details);
router.put('/user/:id', UserController.update);
router.delete('/user/:id', UserController.delete);


router.get('/recipes', RecipeController.findAll);
router.post('/recipes', RecipeController.create);
router.put('/recipes/:id', RecipeController.update);
router.delete('/recipes/:id', RecipeController.delete);

router.get('/favorites', RecipeController.findFavorites);
router.post('/favorites', RecipeController.addFavorite);
router.delete('/favorites/:id', RecipeController.deleteFavorite);

router.post('/ingredient', IngredientController.create);
router.delete('/ingredient/:id', IngredientController.delete);

router.post('/login', UserController.login);
router.post('/search', RecipeController.search);

export default router;