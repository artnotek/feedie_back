// import { Sequelize, Model, DataTypes } from "sequelize";
// const sequelize = new Sequelize('feedie', 'root', '', {
//   host: 'localhost',
//   dialect: 'mysql',
// });
//
// class User extends Model {}
// User.init({
//   firstname: {
//     type: DataTypes.STRING,
//     allowNull: false
//   },
//   lastname: {
//     type: DataTypes.STRING,
//     allowNull: false
//   },
//   email: {
//     type: DataTypes.STRING,
//     allowNull: false
//   },
//   password: {
//     type: DataTypes.STRING,
//     allowNull: false
//   },
//   avatar: {
//     type: DataTypes.STRING,
//     defaultValue: 'avatar.jpg'
//     // allowNull defaults to true
//   }
// }, {
//   sequelize,
//   modelName: 'users'
//   // options
// });
//
// export default User;

import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
  firstname: {
    type: String,
    required: true
  },
  lastname: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true,
    dropDups: true
  },
  password: {
    type: String,
    required: true,
  },
  avatar: {
    type: String,
    default: 'avatar.png'
  }
});

const User = mongoose.model('User', userSchema);
export default User;