// import { Sequelize, Model, DataTypes } from "sequelize";
// import Recipe from "./Recipe";
// const sequelize = new Sequelize('feedie', 'root', '', {
//   host: 'localhost',
//   dialect: 'mysql',
// });
//
// class Instruction extends Model {}
// Instruction.init({
//   recipe_id: {
//     type: DataTypes.INTEGER,
//     allowNull: false,
//       references: {
//       model: 'recipes',
//       key: 'id'
//     }
//   },
//   instruction: {
//     type: DataTypes.STRING,
//     allowNull: false
//   }
// }, {
//   sequelize,
//   modelName: 'instructions'
// });
//
// export default Instruction;

import mongoose from 'mongoose';

const instructionSchema = new mongoose.Schema({
  instruction: {
    type: String,
    required: true
  }
});
const Instruction = mongoose.model('Instruction', instructionSchema);
export default Instruction;